// Class Record has no specialities
public class Record {
    private Song[] songs;
    private String title;
    private int id;


    public Record(String title, int id, Song[] songs) {
        this.songs = songs;
        this.title = title;
        this.id = id;
    }

    public Song[] getSongs() {
        return songs;
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }
}
