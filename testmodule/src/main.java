// The main class is only used for testing
public class main {
    public static void main(String[] args) {
        Record r1 = new Record("unbelievable",1,new Song[]{
                new Song("God sent", 123),
                new Song("Hero arrived", 230),
        });
        Record r2 = new Record("Here we go again",2,new Song[]{
                new Song("The beginning", 74),
                new Song("after", 420),
                new Song("the End", 83)
        });
        Record r3 = new Record("Time to die",3,new Song[]{});
        Record r4 = new Record("Die Too",4, new Song[]{});

        MusicBox mb = new MusicBox();
        mb.addRecord(r1);
        mb.addRecord(r2);
        mb.addRecord(r3);
        mb.addRecord(r4);

        mb.searchRecord("Time to die");
        mb.removeRecord(r3);
        mb.removeRecord(2);
        mb.searchRecord("Time to die");

        mb.getSumOfMusic();

        mb.play(1);
        mb.loadRecord(r2);
        mb.play(1);

        for (int i = 3; i <=52; i++){
            mb.addRecord(new Record("Time to die",i,new Song[]{}));
        }
    }
}
