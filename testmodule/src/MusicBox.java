// Class MusicBox does all the work of the MusicBox

import java.util.ArrayList;

public class MusicBox {
    private ArrayList<Record> records;
    private final int recordNumber = 50;
    private Record loadedRecord;

    // Variables used for different colored text
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";

    public MusicBox() {
        this.records = new ArrayList<>();
    }

    // The addRecord function checks if space is still available and if so it adds the record
    public void addRecord(Record record) {
        if (this.records.size() < recordNumber) {
            this.records.add(record);
            System.out.println(ANSI_GREEN + record.getTitle() + " was added successfully" + ANSI_RESET);
        } else {
            printError("There are to many Records");
        }
    }

    // Here are two different removeRecord functions.
    // This function searches the records List for the same record
    public void removeRecord(Record record) {
        for (Record record1 : records) {
            if (record1 == record) {
                System.out.println(ANSI_GREEN + record.getTitle() + " was removed successfully" + ANSI_RESET);
                if (record1 == loadedRecord) {
                    loadedRecord = null;
                }
                this.records.remove(record1);
                return;
            }
        }
        printError("There is no such Record inserted");
    }

    // This function removes the record at the position rec in the records list
    public void removeRecord(int rec) {
        if (rec < records.size() && rec >= 0) {
            System.out.println(ANSI_GREEN + records.get(rec).getTitle() + " was removed successfully" + ANSI_RESET);
            if (records.get(rec) == loadedRecord) {
                loadedRecord = null;
            }
            records.remove(rec);
        } else if (rec < 0) {
            printError("There can not be negative records");
        } else {
            printError("There are not that many records");
        }
    }

    // Here are two different getSumOfMusic functions.
    // This function adds up all the lengths of the different songs and posts it as minutes and seconds in the Console
    public void getSumOfMusic() {
        int sumOfMusic = 0;
        for (Record record : records) {
            for (Song song : record.getSongs()) {
                sumOfMusic += song.getLength();
            }
        }
        System.out.println("There is a Playtime of: " + sumOfMusic / 60 + " minutes and " + sumOfMusic % 60 + " seconds");
    }

    // The getSumOfMusicCalc function does the same, but returns the playtime in seconds as an int and without posting in the console
    public int getSumOfMusicCalc() {
        int sumOfMusic = 0;
        for (Record record : records) {
            for (Song song : record.getSongs()) {
                sumOfMusic += song.getLength();
            }
        }
        return sumOfMusic;
    }

    // The searchRecord function compares every title of the records in the records list with the given title
    public Record searchRecord(String title) {
        for (Record record : records) {
            if (record.getTitle().equals(title)) {
                return record;
            }
        }
        printError("There is no such record");
        return null;
    }

    // The loadRecord function sets the loadedRecord variable
    public void loadRecord(Record record) {
        loadedRecord = record;
    }

    // The play function posts the title and length of the song in the console and checks for errors
    public void play(int song) {
        if (loadedRecord == null) {
            printError("There is no Record loaded");
        } else if (song >= loadedRecord.getSongs().length) {
            printError("There are not that many songs");
        } else if (song < 1) {
            printError("There can not be negative songs");
        } else {
            System.out.println("The song " + loadedRecord.getSongs()[song - 1].getTitle() +
                    " was played for " + loadedRecord.getSongs()[song - 1].getLength() + " seconds");
        }
    }

    // The prinError function is a simple Error message
    public void printError(String text) {
        System.out.println(ANSI_RED + "        Error");
        System.out.println(text + ANSI_RESET);
    }
}